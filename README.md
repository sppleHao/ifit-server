# iFit-server
第十二届“英特尔杯”全国大学生软件创新大赛iFit项目决赛作品
## Descriptions
iFit项目的server端，提供tensorfolw.js模型文件、视频文件和动作文件，可由web端上传。

本项目基于node.js的express框架，并配置了反向代理可以在单机运行web端和server端，在生产环境如果有需要可以自行配置nginx进行端口转发。
## Usage
### 0.安装npm

### 1.克隆仓库到本地
```sh
git clone https://gitlab.com/sppleHao/ifit-server.git
```

### 3.cd到目录下
```sh
cd <folder-path>
```

### 4.下载依赖

```sh
npm install
```

### 5.配置config.js (在 src/script 文件夹下)

```sh
npm start
```

